var dataModel = {orginatingFlight: {}, data: {}}

dataModel.data.fareFamily=document.querySelector("#irc-flights-panel > panel-irc-flight > irc-flight > div > irc-itinerary:nth-child(1) > div > irc-segment > div.columnFlights.firstAndSecond > div.columnFlights.second > span > div").innerText

dataModel.data.price=document.querySelector("#irc-price-summary-flight-tickets > td.price > span > span.number").innerText

dataModel.orginatingFlight.departure=document.querySelector("#rebook-flight-1").innerText

dataModel.orginatingFlight.depTime=document.querySelector("#irc-flights-panel > panel-irc-flight > irc-flight > div > irc-itinerary:nth-child(1) > div > irc-segment > div.columnFlights.firstAndSecond > div.columnFlights.first > ol > li:nth-child(1) > div.flightData.date > time").innerText

dataModel.orginatingFlight.arrivalTime=document.querySelector("#irc-flights-panel > panel-irc-flight > irc-flight > div > irc-itinerary:nth-child(1) > div > irc-segment > div.columnFlights.firstAndSecond > div.columnFlights.first > ol > li:nth-child(2) > div.flightData.date > time").innerText

var http = new XMLHTTPRequest();

var url = "https://www.api.capturedata.ie/"
http.open('POST', url, true);
http.setRequestHeader('Content-type', 'application/json');
http.send(dataModel)
